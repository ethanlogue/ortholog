/* Copyright 2019 Franklin Harding
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

enum pdx_layers {
  _BASE,
  _RAISE
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_BASE] = LAYOUT(
      LGUI(LCTL(KC_Q)), LCTL(KC_UP), \
      LT(_RAISE,KC__MUTE), KC_MPLY, \
      LCTL(KC_LEFT), LCTL(KC_RIGHT) \
    ),
    [_RAISE] = LAYOUT(
      LALT(LSFT(KC__VOLDOWN)), LALT(LSFT(KC__VOLUP)), \
      _______, KC_MPLY, \
      KC_MRWD, KC_MFFD \
    ),    
};

void matrix_init_user(void) {}

void matrix_scan_user(void) {}

void led_set_user(uint8_t usb_led) {}
