use <utils.scad>
use <key.scad>

nb_row=5;
nb_col=12;
rounding=3;
border=8;
switch_hole=14.3;// by spec should be 14, can be adjusted for printer imprecision
inter_switch=19.05;
back_thickness=1.4;

// insert hole, can be adjusted depending on the size of your insert
// or if you use autotaping screws
insert_diameter=3.2;
insert_height=4.6;

case_depth=12+back_thickness;
case_width=inter_switch*nb_col-(inter_switch-switch_hole)+border*2;
case_height=inter_switch*nb_row-(inter_switch-switch_hole)+border*2;
cut_offset= nb_col%2==0 ? 0 : inter_switch/2;

module key_placement() {
    for (i=[0:nb_col-1]) {
        for (j=[0:nb_row-1]) {
            translate([inter_switch*((nb_col-1)/2-i), inter_switch*(j-(nb_row-1)/2), 0]) {
                children();
            }
        }
    }
}

module hole_placement() {
    b=2.1;
    z_coord=case_depth-back_thickness;
    for (coord=[[ b-case_width/2.1,  b-case_height/2.1, z_coord],
                [ b-case_width/2.1,  -b+case_height/2.1, z_coord],
                [-b+case_width/2.1,  b-case_height/2.1, z_coord],
                [-b+case_width/2.1,  -b+case_height/2.1, z_coord],
                [cut_offset,      -b+case_height/2.1, z_coord],
                [cut_offset,       b-case_height/2.1, z_coord]])
    {
        translate(coord) children();
    }

}

module case() {
    difference() {
        union() {
            linear_extrude(case_depth)
                rounded_square([case_width, case_height], r=rounding, center=true);
        }
        
        // back hole
        translate([0,0,case_depth/2+4])
            cube([case_width-2*border, case_height-2*border, case_depth], center=true);        

        // backpanel pocket
        translate([0,0,case_depth-back_thickness]) linear_extrude(2*back_thickness)
            rounded_square([case_width-2, case_height-2], r=rounding-1, center=true);

        // switch holes
        key_placement() {
            translate([0,0,5]) {
                cube([switch_hole,switch_hole,15], center=true);
                translate([0,0,1.5]) cube([5, switch_hole+3, 10], center=true);
            }
            // chamfer for elephant foots
            translate([0,0,0.4]) rotate([180,0,0]) linear_extrude(switch_hole, scale=3)
              square([switch_hole,switch_hole], center=true);
        }

        // screw holes
        hole_placement() {
          cylinder(d=insert_diameter, h=insert_height*3, center=true);
        }
        

        // second object that will be substracted
        translate([95.25,50,case_depth-back_thickness*3.5])rotate ([90,90,0]) cube(size=[5,10,10], center = true, $fn=100);
        
        
    }
    
    // center brace
    difference() {
        cylinder (h = 12, r=2, center = false, $fn=100);
        rotate ([90,0,0]) cylinder (h = 0, r=1, center = true, $fn=100);
    }
    // center brace
    difference() {
        translate([57.25,0])cylinder (h = 12, r=2, center = false, $fn=100);
        rotate ([90,0,0]) cylinder (h = 0, r=1, center = true, $fn=100);
    }    
    difference() {
        translate([-57.25,0])cylinder (h = 12, r=2, center = false, $fn=100);
        rotate ([90,0,0]) cylinder (h = 0, r=1, center = true, $fn=100);
    }        

}






color([0.3,0.3,0.3]) {
  case();
  back();
}